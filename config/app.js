var express = require('express');
var app = express();
const bodyParser = require('body-parser');

var db = require('./db');
//global.__root   = __dirname + '/src/';

require('./../src/routes/index')(app, express);


app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
  res.header('Access-Control-Expose-Headers', 'Content-Length');
  res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
  if (req.method === 'OPTIONS') {
      return res.send(200);
  } else {
      return next();
  }
});
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.get('/api', function (req, res) {
  res.status(200).send('API works.');
});

//var AuthController = require('./../src/auth/AuthController');
//app.use('/api/auth', AuthController);

//var UserController = require('./../src/controllers/UserController');
//app.use('/api/users', UserController);
//app.get('/api/users', UserController.list);

module.exports = app;