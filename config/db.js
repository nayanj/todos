const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const logger = require('./../src/shared/logger');

mongoose.connect('mongodb://localhost:27017/todos', { useMongoClient: true })
  .then(function () {
    logger.info("Database connected!");
  }).catch(err => {
    logger.error("Database Error! ", err);
  });
