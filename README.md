
# copy/clone the code and run 'npm install' using terminal/commandPrompt

# Create a database named 'todos' using mongoDB

# To create a todos using postman
	Method : POST
	URL: http://localhost:3000/api/v1/todos
	Body: { 
		"name": "Task 1"
		}

# To get all todos using postman
	Method: GET
	URL: http://localhost:3000/api/v1/todos
