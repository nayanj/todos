var app = require('./config/app');
var port = process.env.PORT || 3000;

const { uuid } = require('uuidv4');
const namespace = require('continuation-local-storage').createNamespace('logger');

// Run the context for each request. Assign a unique identifier to each request
app.use(async (req, res, next) => {
  namespace.run(async () => {
    namespace.set('logId', uuid());
    next();
  });
});

var server = app.listen(port, function () {
  console.log('Express server listening on port ' + port);
});