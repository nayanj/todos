var mongoose = require('mongoose');
var TodosSchema = new mongoose.Schema({
  name: String,
  isActive: {
    type: Boolean,
    default: true
  },
  isCompleted: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  createdOn: {
    type: Date,
    default: new Date()
  },
  updatedOn: {
    type: Date
  }
});

mongoose.model('Todos', TodosSchema);
module.exports = mongoose.model('Todos');