module.exports = {
	CREATED: 'Created successfully!',
	UPDATED: 'Updated successfully!',
	DELETED: 'Deleted successfully!',
	REQUIRED: 'Please send required field.',
	TODO_BLANK: 'Todos should not be blank.',
	SOMETHING_WRONG: 'Something went wrong!'
}