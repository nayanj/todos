const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

const Todos = require('./../models/TodosModel');
const constants = require('../shared/constant');


/* exports.list = (req, res) => {
    try {
      let limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 2;
      let page = 0;
      if (req.query) {
        if (req.query.page) {
          req.query.page = parseInt(req.query.page);
          page = Number.isInteger(req.query.page) ? req.query.page : 0;
        }
      }
      
      UserService.list(limit, page).then((result) => {
        res.status(200).json({
                  status: 'success',
                  statusCode: 200,
          message: constants.USER_LIST,
          data: result
              });
      }).catch( error => { res.status(400).send(error);} );
    } catch(error) {		
          res.status(400).send(error);
      } 
};*/


// CREATES A NEW TODOS
router.post('/', async (req, res)=>{
    if (!req.body.name) return res.status(400).send({ status: 400, param: 'name', message: constants.TODO_BLANK });
    Todos.create({
        name: req.body.name
    }, function (err, result) {
        if (err) return res.status(400).send({ status: 400, message: "There was a problem adding the information to the database." });
        res.status(200).send({ data: result });
    });
});

// RETURNS ALL THE TODOS IN THE DATABASE
router.get('/', function (req, res) {
    Todos.find({}, function (err, result) {
        if (err) return res.status(400).send({ status: 400, message: "There was a problem finding the todos." });
        res.status(200).send( { status: 200, data: result });
    });
});


module.exports = router;