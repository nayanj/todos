const TodosController    = require('./../controllers/TodosController');

module.exports = function(app, express) {
  app.use('/api/v1/todos', TodosController);
}

